import {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Shop from './pages/Shop';
import ProductView from './pages/ProductView';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Cart from './pages/Cart';
import Error from './pages/Error';
import AdminDashboard from './pages/AdminDashboard';
import AddProduct from './pages/AddProduct';
import UpdateProduct from './pages/UpdateProduct';
import {UserProvider} from './UserContext';
import { MDBFooter } from 'mdb-react-ui-kit';
import './App.css';

function App() {

    const [user, setUser] = useState({
        id: null,
        isAdmin: null,
        orders: null
    })
        
    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() => {

        fetch('https://mysterious-mesa-58916.herokuapp.com/users/getUserDetails', {
            headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data._id !== "undefined"){
                setUser({
                id: data._id,
                isAdmin: data.isAdmin,
                orders: data.orders
                })
            } else {

                setUser({
                id: null,
                isAdmin: null,
                orders: null
                })
            }
        })
    }, [])

    return (
        <>
        <UserProvider value={{user, setUser, unsetUser}}>
            <Router>
            <AppNavBar/>
                <Routes>
                    <Route exact path="/" element={<Home/>} />
                    <Route exact path="/products/viewAllProducts" element={<Shop/>} />
                    <Route exact path="/products/getSingleProduct/:productId" element={<ProductView/>} />
                    <Route exact path="/users/loginUser" element={<Login/>} />
                    <Route exact path="/logout" element={<Logout/>} />
                    <Route exact path="/users/registerUser" element={<Register/>} />
                    <Route exact path="/users/getAllOrders" element={<Cart/>} />
                    <Route exact path="/AdminDashboard" element={<AdminDashboard/>} />
                    <Route exact path="/AddProduct" element={<AddProduct/>} />
                    <Route exact path="/products/updateProduct/:productId" element={<UpdateProduct/>} />
                    <Route exact path="*" element={<Error/>} />
                </Routes>
            </Router>
        </UserProvider>

        <MDBFooter bgColor='black' className='text-center text-lg-start text-light'>
        <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
            <div className='me-5 d-none d-lg-block'>
            <span>Get connected with us on social networks:</span>
            </div>

            <div>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-facebook-f'></i>
            </a>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-twitter'></i>
            </a>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-google'></i>
            </a>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-instagram'></i>
            </a>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-linkedin'></i>
            </a>
            <a href='#!' className='me-4 text-reset'>
                <i className='fab fa-github'></i>
            </a>
            </div>
        </section>

        <section className=''>
            <div className='container text-center text-md-start mt-5'>
            <div className='row mt-3'>
                <div className='col-md-3 col-lg-4 col-xl-3 mx-auto mb-4'>
                <img src="https://i.ibb.co/SXjWKrg/Screen-Shot-2022-06-21-at-1-26-15-PM.png" alt="icahon-logo"/>
                <p>
                    iChanon is one of the leading and trusted product reseller of Apple products globally.
                </p>
                </div>

                <div className='col-md-2 col-lg-2 col-xl-2 mx-auto mb-4'>
                <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
                <p>
                    <a href='#!' className='text-reset'>
                    Macbook Pro
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Macbook Air
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Iphone
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Accessories
                    </a>
                </p>
                </div>

                <div className='col-md-3 col-lg-2 col-xl-2 mx-auto mb-4'>
                <h6 className='text-uppercase fw-bold mb-4'>Useful links</h6>
                <p>
                    <a href='#!' className='text-reset'>
                    Pricing
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Settings
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Orders
                    </a>
                </p>
                <p>
                    <a href='#!' className='text-reset'>
                    Help
                    </a>
                </p>
                </div>

                <div className='col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4'>
                <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
                <p>
                    <i className='fas fa-home me-3'></i> Candelaria, Quezon Provice 4323, PH
                </p>
                <p>
                    <i className='fas fa-envelope me-3'></i>
                    iCahon@gmail.com
                </p>
                <p>
                    <i className='fas fa-phone me-3'></i> + 63 900 000 000
                </p>
                <p>
                    <i className='fas fa-print me-3'></i> + 63 900 000 000
                </p>
                </div>
            </div>
            </div>
        </section>

        <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
            © 2021 Copyright:
            <a className='text-reset fw-bold' href='#!'>
            iCahon.com
            </a>
        </div>
        </MDBFooter>
        </>

    )
  
}

export default App;
