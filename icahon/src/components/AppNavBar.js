import {useContext} from 'react';
import {Navbar, Container, Nav, Button, Form, FormControl} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';

export default function AppNavBar() {

    const {user} = useContext(UserContext);

    return (

        <Navbar bg="black" variant="dark" expand="lg">
        <Container>
            <Navbar.Brand as={Link} to="/"><img src="https://i.ibb.co/SXjWKrg/Screen-Shot-2022-06-21-at-1-26-15-PM.png" alt="icahon-logo"/></Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ms-auto me-4">
                                   
                <Nav.Link className="me-4" as={Link} to="/">Home</Nav.Link>

                { user.id !== null && user.isAdmin !== true ?
                <>
                <Nav.Link className="me-4" as={Link} to="/products/viewAllProducts">Shop</Nav.Link>
                <Nav.Link className="me-4" as={Link} to="/users/getAllOrders">Cart</Nav.Link>
                </>
                :
                <></>
                }
                { user.id === null ?
                <>
                <Nav.Link className="me-4" as={Link} to="/users/loginUser">Login</Nav.Link>
                <Nav.Link className="me-4" as={Link} to="/users/registerUser">Register</Nav.Link>
                </>
                :
                <></>
                }

                { user.id !== null && user.isAdmin === true ?
                <Nav.Link className="me-4" as={Link} to="/AdminDashboard">Admin Dashboard</Nav.Link>
                :
                <></>
                }

                { user.id !== null ?
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <></>
                }

            </Nav>
                { user.id !== null && !user.isAdmin ?
                <Form className="d-flex mb-3 ms-2">
                    <FormControl
                        type="search"
                        placeholder="Search"
                        className="me-2"
                        aria-label="Search"
                    />
                    <Button variant="light"><i class="fa-solid fa-magnifying-glass"></i></Button>
                </Form>
                :
                <></>
                }
            </Navbar.Collapse>
        </Container>
        </Navbar>
    )
}