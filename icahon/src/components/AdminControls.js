import {Container, Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function AdminControls({prodProp}){

    const {name, description, isActive, price, _id} = prodProp;

    return (
        <Container className="p-0">
            <Table style={{lineHeight:"35px", verticalAlign: 'middle'}} className="dashboard-table bg-light text-center" variant="dark" striped bordered hover size="sm">
                    <thead>
                        <tr className="mb-1">
                            <th width={200}>Product Name</th>
                            <th width={400}>Description</th>
                            <th width={80}>Price</th>
                            <th width={80}>Availability</th>
                            <th width={80}>Action</th>
                        </tr>
                    </thead>
                    <tbody style={{textAlign: 'center'}}>
                        <tr>
                            <td>{name}</td>
                            <td>{description}</td>
                            <td>₱ {price}</td>
                            { isActive === true ?
                            <td><i class="fa-solid fa-circle-check"></i>
                            <hr/>
                            <Link className="btn btn-light" to={`/products/getSingleProduct/${_id}`}>Update</Link>
                            </td>
                            :
                            <td><i class="fa-solid fa-circle-xmark"></i>
                            <hr/>
                            <Link className="btn btn-light" to={`/products/getSingleProduct/${_id}`}>Update</Link>
                            </td>
                            }
                            <td>
                            
                            <Link className="btn btn-light" to={`/products/updateProduct/${_id}`}>Edit</Link>
                            
                            </td>
                        </tr>
                    </tbody>
            </Table>
        </Container>
                            
    )
}