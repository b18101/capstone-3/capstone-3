import {Row, Col, Card, Container} from "react-bootstrap";


export default function Highlight(){
    return (
        <Container className="mb-3">
            <Row>
                <Col className="mb-3" xs={12} md={4}>
                    <Card  className= "all-card p-3 bg-dark">
                    <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2018/10/29/15/27/laptop-3781380_960_720.jpg" />
                        <Card.Body>
                            <Card.Title>
                                <h2>Quality products</h2>
                            </Card.Title>

                            <Card.Text>
                            MacBooks from apple have a design that effortlessly works to match a range of settings. They work as well as they look, but in a way that complements any look. MacBook features a glossy aluminum unibody design that turns heads everywhere.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className="mb-3" xs={12} md={4}>
                    <Card className= "all-card p-3 bg-dark">
                    <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2014/12/30/11/55/office-583839_960_720.jpg" />
                        <Card.Body>
                            <Card.Title>
                                <h2>Delivery is on us</h2>
                            </Card.Title>

                            <Card.Text>
                            Millions of dollars are lost in the time it takes to ship a package, load it on a truck and deliver it to the person who ordered it. iCahon makes the process quicker and easier so you can get your package delivered on time. We will deliver your items efficiently, quickly and right to your front door.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

                <Col className="mb-3" xs={12} md={4}>
                    <Card className= "all-card p-3 bg-dark">
                    <Card.Img variant="top" src="https://cdn.pixabay.com/photo/2020/12/18/16/56/laptop-5842509_960_720.jpg" />
                        <Card.Body>
                            <Card.Title>
                                <h2>Customer Care</h2>
                            </Card.Title>

                            <Card.Text>
                            When your MacBook battery is running a little low, or your MacBook Air or MacBook Pro is giving you any troubles, you need professional help, fast. Hands-on help and friendly advice is what we specialize in. We’re the people you want to get your laptop device or accessory fixed.
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
         </Container>
    )
}