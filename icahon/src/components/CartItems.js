import {Table, Button, Container, Row, Col} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartItems({cartProp}){

    
    const {name, totalAmount, price} = cartProp;
    const history = useNavigate();

// ----------------------- REMOVE ITEM -----------------------------------

const removeItem = () => {

    fetch("https://mysterious-mesa-58916.herokuapp.com/users/getAllUserOrders", {
        
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

        data.forEach(item => {
            if(item.name === name){
                fetch("https://mysterious-mesa-58916.herokuapp.com/users/removeOrder", {
                    method: 'PUT',
                    headers: {
                        'Content-Type' : 'application/json',
                        Authorization: `Bearer ${localStorage.getItem('token')}`
                    },
                    body: JSON.stringify({
                        name: name,
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data){
                        let timerInterval
                        Swal.fire({
                        title: 'Item has been removed',
                        icon: 'success',
                        iconColor: 'black',
                        showConfirmButton: false,
                        imageUrl: "",
                        timer: 1500,
                        

                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                        }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                        })

                        history("/products/viewAllProducts");

                    } else {
                        Swal.fire({
                            title: 'Something went wrong',
                            icon: 'error',
                            text: 'Please try again later'
                        })
                    }
                })
            }
        })
    })
}

    return (
        <Container>
            <Row>
                <Col lg="12">
                    <Table responsive="sm" striped bordered hover size="sm" xs={12} className="bg-light mt-4">
                        <thead>
                            <tr className="mb-5">
                                <th>Product Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{name}</td>
                                <td>{totalAmount}</td>
                                <td>{price}</td>
                                <td><Button variant="danger" onClick={() => removeItem()}>REMOVE</Button></td>
                            </tr>
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>
    )

}
