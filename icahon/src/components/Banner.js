import {Row, Col} from 'react-bootstrap';

export default function Banner({data}){

    const {title, content} = data;

	return (

			<Col className = "p-5">
				<h1 className="banner-text" >{title}</h1>
				<hr className="h-line"/>
				<p className="banner-text" >{content}</p>
				
			</Col>

	)
}