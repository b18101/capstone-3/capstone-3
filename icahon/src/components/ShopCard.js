import {Card, Col, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ShopCard({shopProp}){
    
    const {name, _id} = shopProp;

    return (
    
        <Col className=" card1 mt-4 ms-3">
            <Card className="all-card bg-dark" style={{width: '25rem'}}>
                <Card.Img variant="top" src="https://images.unsplash.com/photo-1527443154391-507e9dc6c5cc?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80" />
                <Card.Body>
                    <Card.Title className="text-center card2 mb-3">{name}</Card.Title>
                    <Link to={`/products/getSingleProduct/${_id}`} style={{ position: "absolute", bottom: 50, right: 140 }}>
                    <Button className="btn btn-view btn-light">View Details</Button>
                    </Link>
                </Card.Body>
            </Card> 
        </Col>


    )

}