import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Carousel from 'react-bootstrap/Carousel';

export default function Home() {
    const data = {
    title: "WELCOME TO iCAHON",
    content: "APPLE PREMIUM RESELLER STORE",
}


    return (
        <>
        <Banner data={data}/>
        <Highlights />
        <Carousel className="carousel-style" fade >
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://cdn.pixabay.com/photo/2016/11/29/06/18/home-office-1867761_960_720.jpg"
              alt="First slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://cdn.pixabay.com/photo/2019/07/14/16/29/pen-4337524_960_720.jpg"
              alt="Second slide"
            />
          </Carousel.Item>
          <Carousel.Item>
            <img
              className="d-block w-100"
              src="https://cdn.pixabay.com/photo/2020/12/18/16/56/laptop-5842509_960_720.jpg"
              alt="Third slide"
            />
          </Carousel.Item>
        </Carousel>
        </>
    )
}