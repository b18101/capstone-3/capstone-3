import {Container, Col, Row,} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import AdminControls from '../components/AdminControls';

export default function AdminDashboard(){

    const [products, setProducts] = useState([])

    useEffect(() => {
        fetch('https://mysterious-mesa-58916.herokuapp.com/products/getAllProducts', {
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            const productsArr = (data.map(product => {
                return(
                    <AdminControls key = {product._id} prodProp = {product} breakpoint={4}/>
                )
            }))

            setProducts(productsArr)
        })

    }, [])

    return (
        <Container className="mt-5 pt-2 ">
            <Row>
                <Col sm={8}>
                <h2 
                className="text-center text-light"
                style={{
                    maxWidth: "1200px",
                    maxHeight: "800px",
                    minWidth: "30px",
                    minHeight: "70px"
                }}
                >ADMIN DASHBOARD</h2>
                <hr className="h-line"/>
                </Col>
                <Col className="mt-3" sm={2}>
                        <Link 
                        to="/AddProduct" 
                        className="btn btn-dark mb-1"
                        style={{
                            fontSize: "20px",
                            fontWeight: 'bold',
                            width: "180px"
                        }}
                        >ADD PRODUCT
                        </Link>
                </Col>
            </Row>
            <Row>               
                <Col lg={12}>
                    {products}
                </Col>
            </Row>
        </Container>

    )

}

