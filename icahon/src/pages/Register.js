import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

    const {user} = useContext(UserContext);

    const history = useNavigate();

    const [userName, setUserName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if((userName !== '' && email !== '' && password !== '') && (password === confirmPassword)) {

            setIsActive(true);

        } else {

            setIsActive(false);

        }
    }, [userName, email, password, confirmPassword]);

    function registerUser(e){

        e.preventDefault();

        fetch('https://mysterious-mesa-58916.herokuapp.com/users/checkEmailExists', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data){

                let timerInterval
                Swal.fire({
                    title: 'Duplicate email found',
                    text: 'Please provide another email',
                    icon: 'info',
                    iconColor: 'black',
                    showConfirmButton: false,
                    imageUrl: "",
                    timer: 1700,
                
                willClose: () => {
                    clearInterval(timerInterval)
                }
                }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
                })
            } else {
                
                fetch('https://mysterious-mesa-58916.herokuapp.com/users/registerUser', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        userName: userName,
                        email: email,
                        password: password
                    })
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    if(data.email){
                        
                        let timerInterval

                        Swal.fire({
                            title: 'Registration successful',
                            icon: 'success',
                            iconColor: 'black',
                            showConfirmButton: false,
                            imageUrl: "",
                            timer: 1500,
                        
                        willClose: () => {
                            clearInterval(timerInterval)
                        }
                        }).then((result) => {
                        if (result.dismiss === Swal.DismissReason.timer) {
                            console.log('I was closed by the timer')
                        }
                        })
                        
                        history("/users/loginUser");

                    } else {
                        Swal.fire({
                            title: 'Registration failed',
                            icon: 'error',
                            text: 'Something went wrong'
                        })
                    }
                })
            }
        })


        setUserName('');
        setEmail('');
        setPassword('');
        setConfirmPassword('');
    }


    return (
        user.id !== null ?
        <Navigate to="/products/viewAllProducts"/>
        :
        <>
        <div class="wrapper">
            <div class="register-container">
                <div class="form">
                    <h2>Register</h2>
                    <hr className="h-line"/>
                    
                    <Form className="mt-5" id="login" onSubmit={e => registerUser(e)}>
                        <Form.Group controlId="userName">
                            <Form.Label>Username</Form.Label>
                            <Form.Control
                                type= "text"
                                placeholder= "@johndoe"
                                required
                                value={userName}
                                onChange={e => setUserName(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mt-2" controlId="userEmail">
                            <Form.Label>Email Address</Form.Label>
                            <Form.Control
                                type= "email"
                                placeholder= "user@mail.com"
                                required
                                value={email}
                                onChange={e => setEmail(e.target.value)}
                            />
                        </Form.Group>

                        <Form.Group className="mt-2" controlId="password">
                            <Form.Label class="password" for="password" id="password">Password:</Form.Label>
                            <Form.Control
                                type= "password"
                                placeholder= "password"
                                required
                                value={password}
                                onChange={e => setPassword(e.target.value)}
                            />
                        </Form.Group>
                        
                        <Form.Group className="mt-2" controlId="confirmPassword">
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control
                                type= "password"
                                placeholder= "confirm Password"
                                required
                                value={confirmPassword}
                                onChange={e => setConfirmPassword(e.target.value)}
                            />
                        </Form.Group>

                    { isActive ?
                        

                        <Button  variant="dark" type="submit" className="mt-3 mb-5" class="login-register-btn">
                        Register
                        </Button>
                        :
                        <Button variant="light" type="submit" className="mt-3 mb-5 bg-dark" class="login-register-btn" disabled>
                        Register
                        </Button>
                    }

                    <p className="have-account">
                        Have an account?
                        <Link className="log-here ms-1 me-1" to="/users/loginUser" variant = "body2" style={{textDecoration: 'none'}}>
                            Login here
                        </Link>
                    </p>
                    </Form>
                </div>
            </div>
        </div>
        </>
    )
}