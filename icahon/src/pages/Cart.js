import {Card, Button, Container, Col, Row} from 'react-bootstrap';
import {useState, useEffect} from 'react';
import {useNavigate} from 'react-router-dom';
import CartItems from '../components/CartItems';
import Swal from 'sweetalert2';

export default function Cart({product}){

    const [items, setItems] = useState([])
    const [itemNames, setItemNames] = useState([])
    const [prices, setPrices] = useState([])

    const history = useNavigate();

    useEffect(() => {
        fetch("https://mysterious-mesa-58916.herokuapp.com/users/getAllUserOrders", {
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {


            const productsArr = (data.map(product => {
                return(
                    <CartItems key = {product._id} cartProp = {product} breakpoint={4}/>
                )
            }))

            setItems(productsArr)

            setItemNames(data.map(item => {
                return (item.name)
            }))

            setPrices(data.map(item => {
                return (item.price)
            }))
        })
    }, [])

// ----------------------- REMOVE ITEM AFTER CHECKOUT -----------------------------------

const checkOut = () => {

    fetch("https://mysterious-mesa-58916.herokuapp.com/users/getAllUserOrders", {    
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

        if(data.length > 0){

            fetch("https://mysterious-mesa-58916.herokuapp.com/users/removeAllOrder", {
                method: 'PUT',
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
            })
            .then(res => res.json())
            .then(data => {

                if(data.length === 0){

                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again later'
                    })

                } else {
                    let timerInterval
                    Swal.fire({
                    title: 'Checkout successful',
                    icon: 'success',
                    iconColor: 'black',
                    showConfirmButton: false,
                    imageUrl: "",
                    timer: 1500,
                    

                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                    }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                    })

                    history("/")
                }

            })
        }
    })

}

//--------------------------- CANCEL ORDER BUTTON -----------------------------------------------

const cancelOrder = () => {

    fetch("https://mysterious-mesa-58916.herokuapp.com/users/getAllUserOrders", {    
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {

        if(data.length > 0){

            fetch("https://mysterious-mesa-58916.herokuapp.com/users/removeAllOrder/", {
                method: 'PUT',
                headers: {
                    'Content-Type' : 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                },
            })
            .then(res => res.json())
            .then(data => {

                if(data.length === 0){

                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again later'
                    })

                } else {
                    let timerInterval
                    Swal.fire({
                    title: 'Order cancelled',
                    icon: 'success',
                    iconColor: 'black',
                    showConfirmButton: false,
                    imageUrl: "",
                    timer: 1500,
                    

                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                    }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                    })

                    history("/products/viewAllProducts")
                }

            })
        }
    })

}

// ----------------------- GETTING THE SUM OF PRICES FROM USEEFFECT----------------------------------

    const sumPrices = prices.reduce((accumulator, currentValue) => {
            return accumulator + currentValue;
    }, 0);
    
//---------------------------------------------------------------------------------------------------
    return(
        <Container className="cart-body mt-5 mb-5">
            <Row>
                <Col sm={12} className="cart-orders mt-3 text-center">
                <h1>Your Orders</h1>
                <hr className="h-line"/>
                </Col>

                <Col sm={6} md={10} lg={6} className="cart-orders ms-5 ps-5 mt-2 mb-2 text-center">
                    {items}
                </Col>
                

                
                <Col className="ms-5 ps-4">
                    <div class="cart-container">
                        <div class="cart-card">
                        <img src="https://cdn.pixabay.com/photo/2016/12/21/16/34/shopping-cart-1923313_960_720.png" alt="cart-img" class="card__hero" />
                        <div class="card__content">
                            <h2 class="content__title">Order Summary</h2>
                            <p class="content__desc">Review the following order details before checking out.</p>
                            
                                <Card>
                                    <Row className="checkout-item mt-2">
                                    <div>{itemNames.map(name =>
                                    <h6 key={name.toString()}>
                                        {name}
                                    </h6>)}
                                    </div>
                                    </Row>
                                    <Row className="mb-2">
                                    <div style={{fontWeight: 'bold'}}>Total Price:</div>
                                    <div>₱ {sumPrices}</div>                               
                                    </Row>
                                </Card>
                            

  
                            <div class="cart-footer">
                            <Button className="mb-3" variant="dark" onClick={() => checkOut()}>Proceed to payment</Button><br/>
                            <Button variant="danger" onClick={() => cancelOrder()}>Cancel Order</Button><br/>
                            </div>
                        </div>
                        </div>
                    </div>                        
                </Col>
            </Row>
        </Container>


    )
}

