import {useState} from 'react';
import {Form, Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AddProduct(){

    const history = useNavigate();

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    function addProduct(e){

        e.preventDefault();

        fetch("https://mysterious-mesa-58916.herokuapp.com/products/createProduct", {
            method: 'POST',
            headers: {
                'Content-Type' : 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })

        })
        .then(res => res.json())
        .then(data => {
            
            if(data.name !== null){

                let timerInterval

                Swal.fire({
                  title: 'Product has been added',
                  icon: 'success',
                  iconColor: 'black',
                  showConfirmButton: false,
                  imageUrl: "",
                  timer: 1500,
              
                willClose: () => {
                    clearInterval(timerInterval)
                }
                }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
                })
    
                history("/AdminDashboard");
    
            } else {
                Swal.fire({
                    title: 'Something went wrong',
                    icon: 'error',
                    text: 'Please try again later'
                })
            }
        })

    }

    return (
      
      <div  class="wrapper">
            <div  style={{width: "400px"}}class="register-container">
                <div  class="form">
                    <h2 className="mt-3">Add Product</h2>
                    <hr className="h-line"/>
        <Form style={{width: "360px"}} className="mt-3" onSubmit={e => addProduct(e)}>
          <Form.Group className="mb-3" controlId="formProductName">
            <Form.Label>Product Name</Form.Label>
            <Form.Control type="text" onChange={e => setName(e.target.value)} required/>
          </Form.Group>
    
          <Form.Group className="mb-3" controlId="formProductDescription">
            <Form.Label>Description</Form.Label>
                 <div>
                 <textarea style={{height: "200px", width: "360px"}}
                 onChange={e => setDescription(e.target.value)}
                 required
                 ></textarea>
                 </div>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formPrice">
            <Form.Label>Price</Form.Label>
            <Form.Control type="text" onChange={e => setPrice(e.target.value)}  required/>
          </Form.Group>

          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Save Product" required/>
          </Form.Group>

          <Button variant="dark" type="submit">
            Submit
          </Button>
        </Form>
        </div>
        </div>
        </div>

  )
}
