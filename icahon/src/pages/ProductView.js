import {useState, useEffect, useContext} from 'react';
import {Container, Row, Col, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import Carousel from 'react-bootstrap/Carousel';

export default function ProductView() {

    const {user} = useContext(UserContext);

    const history = useNavigate();

    const {productId} = useParams();

// ---------------------- FOR NON-ADMIN USERS --------------------------------
    const [addedItem, setAddedItem] = useState(0)
    const [availableItem, setAvailableItem] = useState(5)
    const [isOpen, setIsOpen] = useState(false)
    const [isOpen1, setIsOpen1] = useState(true)
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);


// ------------ CREATE AN ORDER BY CLICKING ON ADD TO CART BUTTON ------------------
    const addToCart = (productId) => {

                if(addedItem === 0){
                    let timerInterval
                    Swal.fire({
                    title: 'Please add an item',
                    icon: 'warning',
                    iconColor: 'black',
                    showConfirmButton: false,
                    imageUrl: "",
                    timer: 1200,
                    

                    willClose: () => {
                        clearInterval(timerInterval)
                    }
                    }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                        console.log('I was closed by the timer')
                    }
                    })

                } else {

                    fetch(`https://mysterious-mesa-58916.herokuapp.com/users/createUserOrder/${productId}`, {

                        method: 'POST',
                        headers: {
                            'Content-Type' : 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('token')}`
                        },
                        body: JSON.stringify({
                            name: name,
                            totalAmount: addedItem,
                            price: price
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);

                        if(data){

                            let timerInterval
                            Swal.fire({
                            title: 'Successfully added to cart',
                            icon: 'success',
                            iconColor: 'black',
                            showConfirmButton: false,
                            imageUrl: "",
                            timer: 1500,
                            

                            willClose: () => {
                                clearInterval(timerInterval)
                            }
                            }).then((result) => {
                            if (result.dismiss === Swal.DismissReason.timer) {
                                console.log('I was closed by the timer')
                            }
                            })

                            history("/products/viewAllProducts");

                        } else {
                            Swal.fire({
                                title: 'Something went wrong',
                                icon: 'error',
                                text: 'Please try again later'
                            })
                        }
                    })
                }
            //     }
            // }

    //     })
	}

// --------------------- NON-ADMIN USER ADD AND DEDUCT QUANTITY -------------------------------
  

    const addItem = () => { 
        setAddedItem(addedItem + 1);
        setAvailableItem(availableItem - 1);
    }

    const removeItem = () => {   
        setAddedItem(addedItem - 1);
        setAvailableItem(availableItem + 1);
    }

    useEffect(() => {
        if(availableItem === 0) {
            setIsOpen(true)
        } else {
            setIsOpen(false)
        }
    }, [availableItem])

    useEffect(() => {
        if(addedItem > 0) {
            setIsOpen1(false)
        } else {
            setIsOpen1(true)
        }
    }, [addedItem])


// --------------------- RETREVING SINGLE PRODUCT ------------------------------
    useEffect(() => {

        fetch(`https://mysterious-mesa-58916.herokuapp.com/products/getSingleProduct/${productId}`)
        .then(res => res.json())
        .then(data => {

            setName(data.name)
            setPrice(data.price)
            setDescription(data.description)

        })

    }, [productId])

// --------------------- ADMIN BUTTONS ------------------------------------------

// --------------------- DISABLE FUNCTION ---------------------------------------

const archiveProduct = (productId) => {

    fetch(`https://mysterious-mesa-58916.herokuapp.com/products/archiveProduct/${productId}`, {
        method: "PUT",
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        if(!data.isActive){

            let timerInterval;
            Swal.fire({
                title: 'Product has been archived',
                icon: 'success',
                iconColor: 'black',
                showConfirmButton: false,
                imageUrl: "",
                timer: 1500,
            
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }
            })

            history("/AdminDashboard");

        } else {
            Swal.fire({
                title: 'Something went wrong',
                icon: 'error',
                text: 'Please try again later'
            })

            history("/AdminDashboard");
        }        
    })
}

// --------------------- ENABLE FUNCTION ---------------------------------------

const activateProduct = (productId) => {

    fetch(`https://mysterious-mesa-58916.herokuapp.com/products/activateProduct/${productId}`, {
        method: "PUT",
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        }
    })
    .then(res => res.json())
    .then(data => {
        
        if(data.isActive){


            let timerInterval
            Swal.fire({
                title: 'Product has been activated',
                icon: 'success',
                iconColor: 'black',
                showConfirmButton: false,
                imageUrl: "",
                timer: 1500,
            
            willClose: () => {
                clearInterval(timerInterval)
            }
            }).then((result) => {
            if (result.dismiss === Swal.DismissReason.timer) {
                console.log('I was closed by the timer')
            }
            })

            history("/AdminDashboard");

        } else {
            Swal.fire({
                title: 'Something went wrong',
                icon: 'error',
                text: 'Please try again later'
            })
        }
    })
}

    return (

        <Container className="mt-1 mb-3 pt-3 pb-3 productContainer">
            <Row>
                <Col className="mb-2" sm={12} md={6} lg={4}>
                    <Card className="all-card bg-dark p-2 bg-light text-light">
                        <Card.Body>
                            <Card.Title className="mb-4" style={{fontWeight: 'bold'}}>{name}</Card.Title>
                            <hr className="h-line mb-4"/>
                            <Card.Subtitle className="mb-2" style={{fontWeight: 'bold'}}>Description</Card.Subtitle>
                            <Card.Text className="mb-4">{description}</Card.Text>

                            <Card.Subtitle style={{fontWeight: 'bold'}}>Price</Card.Subtitle>
                            <Card.Text className="mb-4">₱ {price}</Card.Text>
                            {user.id !== null && user.isAdmin !== true ?
                            <>
                            <Row>
                            <Col>
                            <Card.Subtitle style={{fontWeight: 'bold'}}>Quantity</Card.Subtitle>
                            <Card.Text className="ms-4">{addedItem}</Card.Text>
                            </Col>
                            <Col>
                            <Button style={{width: '60px'}} variant="light" onClick={removeItem} disabled={isOpen1}>-</Button>
                            </Col>
                            <Col>
                            <Button style={{width: '60px'}} variant="light" onClick={addItem} disabled={isOpen}>+</Button>
                            </Col>
                            </Row>
                            </>
                            :
                            <></>
                            }
                        </Card.Body>

                            {user.id !== null && user.isAdmin !== true ?
                            <Button className="btn-view" variant="light" onClick={() => addToCart(productId)}>ADD TO CART</Button>
                            :
                            <>
                            <Row>
                            <Col>
                            <Button className="ms-4 mb-3" variant="light" onClick={() => activateProduct(productId)} >Enable</Button>
                            </Col>
                            <Col>
                            <Button className="me-1" variant="warning" onClick={() => archiveProduct(productId)}>Disable</Button>
                            </Col>
                            </Row>
                            </>
                            }   

                            {user.id === null ?
                            <Link className="btn btn-danger" to="/users/loginUser">Log In</Link>
                            :
                            <></>
                            }
                    </Card>                   
                </Col>
                <Col>
                    <Carousel fade>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="https://cdn.pixabay.com/photo/2020/10/21/18/07/laptop-5673901_960_720.jpg"
                            alt="First slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="https://cdn.pixabay.com/photo/2019/04/25/14/43/workplace-4155023_960_720.jpg"
                            alt="Second slide"
                            />
                        </Carousel.Item>
                        <Carousel.Item>
                            <img
                            className="d-block w-100"
                            src="https://cdn.pixabay.com/photo/2016/10/11/09/26/office-1730940_960_720.jpg"
                            alt="Third slide"
                            />
                        </Carousel.Item>
                    </Carousel>
                </Col>
            </Row>
        </Container>
    )
}