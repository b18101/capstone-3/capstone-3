import {useState, useEffect, useContext} from 'react';
import {Form, Button } from "react-bootstrap";
import {Navigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login(props){

    const {user, setUser} = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function loginUser(e){

        e.preventDefault();

        fetch('https://mysterious-mesa-58916.herokuapp.com/users/loginUser', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(typeof data.accessToken !== "undefined"){
                localStorage.setItem('token', data.accessToken);
                retrieveUserDetails(data.accessToken);

                let timerInterval
                Swal.fire({
                    title: 'Login Successful',
                    icon: 'success',
                    iconColor: 'black',
                    showConfirmButton: false,
                    imageUrl: "",
                    timer: 1500,
                
                willClose: () => {
                    clearInterval(timerInterval)
                }
                }).then((result) => {
                if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
                })


            } else {
                
                Swal.fire({
                    title: 'Login Failed',
                    icon: 'error',
                    text: 'Check your credentials and try again',
                    iconColor: 'black',
                    showConfirmButton: false,
                    timer: 1500,
                })
            }
        })

        setEmail('');
        setPassword('');
    }

    const retrieveUserDetails = (token) => {

        fetch('https://mysterious-mesa-58916.herokuapp.com/users/getUserDetails', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {

            setUser({
                id: data._id,
                isAdmin: data.isAdmin,
            })
        })
    }


    useEffect(() => {
        if((email !== '' && password !== '')){

            setIsActive(true);

        } else {

            setIsActive(false);

        }
    }, [email, password]);

    return (
        (user.id !== null) ?
        <Navigate to="/"/>
        :
        <>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        <div class="wrapper">
        <div class="login-container">
            <div class="form">
            <h2>Login</h2>
            <hr className="h-line"/>
            <Form className="mt-4" onSubmit={e => loginUser(e)}>
                <Form.Group controlId="loginEmail">
                    <Form.Label class="email" for="login" id="login">Email Address:</Form.Label>
                        <Form.Control
                            type= "email"
                            placeholder= "user@mail.com"
                            required
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                        />
                </Form.Group>

                <Form.Group controlId="loginPassword">
                    <Form.Label class="password" for="password" id="password">Password:</Form.Label>
                    <Form.Control
                        type= "password"
                        placeholder= "*********"
                        required
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                    />
                </Form.Group>

                { isActive ?
                    <Button  variant="dark" type="submit" id="loginBtn" className="mt-3 mb-5" class="login-register-btn">
                        Sign in
                    </Button>

                    :

                    <Button variant="light" type="submit" id="loginBtn" className="mt-3 mb-5 bg-dark" class="login-register-btn" disabled>
                        Sign in
                    </Button>
                }
            </Form>
            <a href='#!' class="facebook"><span class="icon"><i class="fa fa-facebook-f"></i></span> Signin with facebook</a>
            <a href='#!' class="twitter"><span class="icon"><i class="fa fa-twitter"></i></span> Signin with Twitter</a>
            </div>
        </div>
        </div>
        </>
    )
}